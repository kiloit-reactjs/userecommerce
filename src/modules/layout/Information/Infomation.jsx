import { useEffect, useState } from "react";
import { IoTimeOutline } from "react-icons/io5";
import { PiPhoneCallThin } from "react-icons/pi";
const Information = () => {
  const [currentDate, setCurrentDate] = useState(new Date());

  useEffect(() => {
    // Update the current date every second
    const intervalId = setInterval(() => {
      setCurrentDate(new Date());
    }, 1000);

    // Clean up the interval on component unmount
    return () => clearInterval(intervalId);
  }, []); // Empty dependency array means this effect runs only once after initial render
  return (
    <div className="row py-2 px-5 text-light bg-dark">
      <div className="col-lg-6 col-md-12 col-sm-12 d-flex justify-content-start  ">
        <div className="d-flex align-items-center">
          <PiPhoneCallThin className="fs-3" />
          <p className="m-auto">We are available 24/7, Need help? +099949343</p>
        </div>
      </div>
      <div className="col-lg-6 col-md-12 col-sm-12 d-flex justify-content-end ">
        <div className="d-flex align-items-center">
          <p className="m-auto ms-2">{currentDate.toLocaleString()}</p>
          <IoTimeOutline className="fs-3" />
        </div>
      </div>
    </div>
  );
};
export default Information;
