import React from "react";
import { Outlet } from "react-router-dom";
import Navbar from "./navigation/Navbar";

const Layout = () => {
  return (
    <div className="container-fluid m-0 p-0">
      <div>
        <Navbar />
        <Outlet />
      </div>
    </div>
  );
};

export default Layout;
