import React from "react";

import Information from "../Information/Infomation";
import NavbarLg from "./navbarLg";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="row">
      <div className="col-lg-12 bg-dark">
        <Information />
      </div>
      <li className="nav-item">
        <Link
          className="nav-link"
          onClick={() => {
            localStorage.removeItem("token");
            window.location.reload();
          }}
        >
          Logout
        </Link>
      </li>
      <NavbarLg />
      {/* <nav className="navbar navbar-expand-lg fw-bold d-flex justify-content-between px-5">
        <ul className="navbar-nav ">
          <li className="nav-item dropdown">
            <Link
              className="nav-link dropdown-toggle"
              type="button"
              data-bs-toggle="dropdown"
              aria-expanded="false"
              to="/product"
            >
              Categories
            </Link>
            <ul className="dropdown-menu">
              <li>
                <Link className="dropdown-item">Shoes</Link>
              </li>
              <li>
                <Link className="dropdown-item">Shoes</Link>
              </li>
              <li>
                <Link className="dropdown-item">Shoes</Link>
              </li>
            </ul>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/">
              About Us
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/">
              Contact Us
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="*">
              Not found
            </Link>
          </li>
         
        </ul>
        <ul className="navbar-nav ">
          <li className="nav-item">
            <Link className="nav-link" to="/">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/product">
              Category Product
            </Link>
          </li>
        </ul>
      </nav> */}
    </div>
  );
};

export default Navbar;
