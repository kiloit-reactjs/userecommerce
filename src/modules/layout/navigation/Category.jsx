import React from "react";
import Dropdown from "react-bootstrap/Dropdown";
const CategoryDropDown = () => {
  return (
    <Dropdown>
      <Dropdown.Toggle className="btn btn-light fw-bold" id="dropdown-basic">
        Category
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default CategoryDropDown;
