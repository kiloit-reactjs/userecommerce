import React from "react";
import { CiSearch, CiShoppingCart } from "react-icons/ci";
import { FiUser } from "react-icons/fi";
import Logo from "../../../assets/image/KiloIT-Logo-Final-01.png";
import CategoryDropDown from "./Category";
import { Link } from "react-router-dom";
const NavbarLg = () => {
  return (
    <nav className="navbar navbar-expand-lg d-none d-lg-flex justify-content-evenly">
      <div>
        {" "}
        <img style={{ width: "80px" }} src={Logo} alt="" />
        <Link
          to="/"
          className="navbar-brand fw-bold text-start"
          style={{ color: "#3a5a40" }}
        >
          KiloEcommerce
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
      </div>
      <div>
        <ul className="navbar-nav text-dark fw-bold ">
          <li className="nav-item">
            <CategoryDropDown className="nav-link" />
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/product-item">
              Our Product
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link">What's new ?</Link>
          </li>
        </ul>
      </div>
      <div>
        <ul className="navbar-nav text-dark fw-bold d-flex align-items-center">
          <li className="nav-item">
            <div className="d-flex align-items-center form-control rounded-5 bg-light">
              <input
                className="form-control border border-0 bg-light rounded-5"
                placeholder="Search Product"
                type="search"
              />
              <CiSearch className="fs-1" />
            </div>
          </li>
          <li className="nav-item">
            <Link className="nav-link">
              <div className="d-flex">
                <FiUser className="fs-3" />
                <p className="m-auto">Account</p>
              </div>
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link">
              <div className="d-flex ">
                <CiShoppingCart className="fs-3" />
                <p className="m-auto">Cart</p>
              </div>
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default NavbarLg;
