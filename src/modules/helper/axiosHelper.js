// axiosSetup.js
import axios from "axios";

export const setUpAxios = () => {
  axios.defaults.baseURL = "http://13.214.207.172:8003/api/v1/"; // Relative URL for proxy server
  axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token");
};
