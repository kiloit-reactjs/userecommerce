import axios from "axios";

const reqProductItem = () => {
  return axios.get("product-items?");
};
const reProductDetail = (id) => {
  return axios.get(`product-items/` + id);
};
export { reqProductItem, reProductDetail };
