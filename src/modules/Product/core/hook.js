import { useDispatch } from "react-redux";
import { reProductDetail, reqProduct, reqProductItem } from "./request";
import { setListProduct, setProductDetail } from "./productSlice";

const useProduct = () => {
  const dispatch = useDispatch();
  const getProduct = () => {
    reqProductItem().then((res) => {
      dispatch(setListProduct(res.data.data));
      console.log(res.data.data);
    });
  };
  const getProductDetail = (id) => {
    return reProductDetail(id)
      .then((response) => {
        console.log(response.data.data);
        dispatch(setProductDetail(response.data.data));
      })
      .catch((err) => {
        alert(err.message);
      });
  };
  return { getProduct, getProductDetail };
};
export { useProduct };
