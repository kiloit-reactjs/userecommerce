import { createSlice } from "@reduxjs/toolkit";

const productSlice = createSlice({
  name: "products",
  initialState: {
    listProduct: [],
    productDetail: [],
  },
  reducers: {
    setListProduct: (state, action) => {
      state.listProduct = action.payload;
    },
    setProductDetail: (state, action) => {
      state.productDetail = action.payload;
    },
  },
});
export const { setListProduct, setProductDetail } = productSlice.actions;
export default productSlice.reducer;
