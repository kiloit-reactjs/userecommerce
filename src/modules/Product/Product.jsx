import React, { useEffect } from "react";
import { useProduct } from "./core/hook";
import { useSelector } from "react-redux";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
const Product = () => {
  const { getProduct } = useProduct();
  const { listProduct } = useSelector((state) => state.products);
  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div className="row">
      <h1 className="text-center">Our Product</h1>
      {listProduct.map((items) => {
        return (
          <div className="col-lg-3 d-flex justify-content-center my-3">
            <Card className="" style={{ width: "25rem" }}>
              <Card.Img
                variant="top"
                style={{ height: "60%" }}
                src={items.image}
              />
              <Card.Body className="d-flex flex-column">
                <div className="d-flex justify-content-between align-items-center  ">
                  <Card.Title className="fs-6">{items.product.name}</Card.Title>
                  <Card.Text className="text-dark fw-bold fs-5">
                    $ {items.price}
                  </Card.Text>
                </div>
                <Card.Text className="text-secondary mt-4">
                  {items.code}
                </Card.Text>
                <Link className="m-auto" to={`/product-item/${items.id}`}>
                  <button className="btn btn-dark">Product Detail</button>
                </Link>
              </Card.Body>
            </Card>
          </div>
        );
      })}
    </div>
  );
};

export default Product;
