import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router";
import { useProduct } from "../core/hook";
import { useSelector } from "react-redux";
import { FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";

const ProductDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { getProductDetail } = useProduct();
  const { productDetail } = useSelector((state) => state.products);
  const { token } = useSelector((state) => state.auth);

  useEffect(() => {
    getProductDetail(id);
  }, []);

  const handleButtonClick = () => {
    if (token) {
      navigate("/shopping-cart");
    } else {
      navigate("/login");
    }
  };

  return (
    <div className="row p-5 ">
      <div
        className="d-flex justify-content-evenly p-5 bg-light rounded-4"
        style={{
          boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
        }}
      >
        <div className="position-relative">
          <span className="badge text-bg-primary rounded-3  position-absolute m-2 px-2">
            {productDetail &&
            productDetail.product &&
            productDetail.product.category.name
              ? productDetail.product.category.name
              : "Loading"}
          </span>
          <img
            className="w-75 "
            src={productDetail ? productDetail.image : "Loading"}
            alt=""
          />
        </div>
        <div className="">
          <p className="fs-6 text-dark mt-3">
            {productDetail &&
            productDetail.product &&
            productDetail.product.name
              ? productDetail.product.name
              : "Loading"}
          </p>
          <p className="fs-4 fw-bold text-dark mt-3">
            {productDetail ? productDetail.code : "Loading"}
          </p>
          <p className="fs-6 text-dark mt-3">
            {productDetail &&
            productDetail.product &&
            productDetail.product.description
              ? productDetail.product.description
              : "Loading"}
          </p>
          <div className="d-flex text-dark align-items-center text-start">
            <p className="fs-4">
              Price :{" "}
              <span className="fw-bold">
                $ {productDetail ? productDetail.price : "Loading"}
              </span>
            </p>
            <p className="fs-4 ms-5">
              Quantity:{" "}
              <span className="fw-bold">
                {productDetail ? productDetail.quantity : "Loading"}
              </span>
            </p>
          </div>
          <div className="d-flex mt-4">
            <button onClick={handleButtonClick} className="btn btn-success">
              <FiPlus className="fs-5 me-2" />
              Go to shopping carts
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
