import { useDispatch } from "react-redux";
import { reqLogin } from "./request";
import { setToken } from "./authSlice";

const useAuth = () => {
  const dispatch = useDispatch();

  const onLogin = (payload) => {
    reqLogin(payload).then((res) => {
      const data = res.data.data;
      dispatch(setToken(data.token));
    });
  };

  return { onLogin };
};

export { useAuth };
