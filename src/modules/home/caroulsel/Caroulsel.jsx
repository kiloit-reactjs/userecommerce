import React from "react";
import Carousel from "react-bootstrap/Carousel";
import { Link } from "react-router-dom";

const BannerHome = () => {
  const sideData = [
    { id: 1, img: require("../../../assets/image/1.png") },
    { id: 2, img: require("../../../assets/image/2.png") },
    { id: 3, img: require("../../../assets/image/3.png") },
  ];
  return (
    <Carousel>
      {sideData.map((item) => {
        return (
          <Carousel.Item>
            <img className="w-100" src={item.img} alt="" />
            <Carousel.Caption className="text-center">
              <Link to="/product-item">
                <button
                  className="btn btn-dark fw-bold"
                  style={{ top: "-50%" }}
                >
                  See more product
                </button>
              </Link>
            </Carousel.Caption>
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
};

export default BannerHome;
