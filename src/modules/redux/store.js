import { configureStore } from "@reduxjs/toolkit";
import authSlice from "../auth/core/authSlice";
import productSlice from "../Product/core/productSlice";

export const store = configureStore({
  reducer: { auth: authSlice, products: productSlice },
});
