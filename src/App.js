import { BrowserRouter, Route, Routes } from "react-router-dom";
import Homepage from "./modules/home/Homepage";
import Login from "./modules/auth/components/Login";
import Layout from "./modules/layout/Layout";
import NotFoundPage from "./modules/notfoundpage/notfoundpage";
import { useSelector } from "react-redux";
import Product from "./modules/Product/Product";
import "bootstrap/dist/js/bootstrap.js";
import "../src/assets/css/index.css";
import ProductDetail from "./modules/Product/components/ProductDetail";
import ShoppingCart from "./modules/ShoppingCart";

function App() {
  const { token } = useSelector((state) => state.auth);
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {token ? (
            <Route path="/" element={<Layout />}>
              <Route index element={<Homepage />} />
              <Route path="/product-item" element={<Product />} />
              <Route path="/product-item/:id" element={<ProductDetail />} />
              <Route path="/shopping-cart" element={<ShoppingCart />} />
              <Route path="*" element={<NotFoundPage />} />
            </Route>
          ) : (
            <Route path="*" element={<Login />} />
          )}
        </Routes>
      </BrowserRouter>
    </div>
  );
}
export default App;
